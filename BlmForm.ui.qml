import QtQuick 2.11
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0

Item {
   id: item1
   width: 200
   height: 180
   scale: 1

   property alias countLabel: countLabel.text
   property alias maxCountlabel: maxCountlabel.text
   property alias deviceName: deviceNamelabel.text
   property alias blmStateImage: blmStateImage.source
   property alias specialistMode: specialistRow.visible
   property AppModel appModel

   Rectangle {
      id: rectangle
      color: "#ffffff"
      radius: 15
      border.width: 2
      anchors.fill: parent
   }

   RectangularGlow {
      id: effect
      anchors.fill: rectangle
      glowRadius: 10
      spread: 0.2
      color: "white"
      cornerRadius: rectangle.radius + glowRadius
   }

   Item {
      id: column
      anchors.rightMargin: 10
      anchors.leftMargin: 10
      anchors.bottomMargin: 10
      anchors.topMargin: 10
      anchors.fill: parent

      Label {
         id: deviceNamelabel
         text: qsTr("XX.YYY")
         anchors.top: parent.top
         anchors.topMargin: 0
         verticalAlignment: Text.AlignVCenter
         horizontalAlignment: Text.AlignHCenter
         font.pointSize: 13
         font.bold: true
         anchors.right: parent.right
         anchors.rightMargin: 0
         anchors.left: parent.left
         anchors.leftMargin: 0
      }

      Image {
         id: blmStateImage
         width: 100
         height: 100
         anchors.horizontalCenter: parent.horizontalCenter
         anchors.top: deviceNamelabel.bottom
         anchors.topMargin: 5
         source: "images/Ok.png"
      }

      Row {
         id: specialistRow
         anchors.right: parent.horizontalCenter
         anchors.rightMargin: -56
         anchors.left: parent.horizontalCenter
         anchors.leftMargin: -56
         anchors.top: blmStateImage.bottom
         anchors.topMargin: 0
         spacing: 2

         TextField {
            id: textField
            width: 50
            height: 20
            text: qsTr("")
            font.pointSize: 9
            leftPadding: 2
            padding: 1
         }

         Button {
            id: setButton
            width: 30
            height: 20
            text: qsTr("Set")
            rightPadding: 0
            leftPadding: 0
            padding: 0
            spacing: 0
         }

         Button {
            id: initButton
            width: 30
            height: 20
            text: qsTr("Init")
            rightPadding: 0
            leftPadding: 0
            padding: 0
         }
      }

      Row {
         id: countLabels
         spacing: 5
         anchors.horizontalCenterOffset: 0
         anchors.top: specialistRow.bottom
         anchors.topMargin: 2
         anchors.horizontalCenter: parent.horizontalCenter

         Label {
            id: countLabel
            color: "#78ade3"
            text: qsTr("???")
            font.pointSize: 13
            font.bold: true
         }

         Label {
            id: label
            text: qsTr("/")
            font.pointSize: 13
            font.bold: true
         }

         Label {
            id: maxCountlabel
            color: "#8e6108"
            text: qsTr("???")
            font.bold: true
            font.pointSize: 13
         }
      }
   }
}
