import QtQuick 2.4

LedPanelForm {

   //   onWidthChanged: console.log("width=" + width)
   onBlmStatusBoolArrayChanged: {
      // check if blmStatusBoolArray is defined
      if (blmStatusBoolArray) {
         // use blmLedRepeater and not blmStatusBoolArray length as the last value is a spare and therfore not displayed
         for (var index = 0; index < blmLedRepeater.count; index++) {
            //console.log("Received[" + index + "]= " + blmStatusBoolArray[index])
            if (blmStatusBoolArray[index] !== null) {
               // TODO: remove this line when fully tested
               // if (index % 2 == 0)
               blmLedRepeater.itemAt(
                        index).blmStatus = blmStatusBoolArray[index]
            }
         }
      }
   }
}
