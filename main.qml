import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls 1.4 as QT14
import QtQuick.Layouts 1.3

ApplicationWindow {
   id: applicationWindow
   title: qsTr("Qt PSB BLM Application")
   visible: true
   readonly property int windowWidthExpanded: 1900
   readonly property int windowHeightExpanded: 950
   readonly property int windowWidthCompact: 200
   readonly property int windowHeightCompact: 80

   AppModel {
      id: applicationModel

      onUseCompactViewChanged: {
         console.log("useCompactView=" + useCompactView)
         applicationWindow.width = useCompactView ? windowWidthCompact : windowWidthExpanded
         applicationWindow.height = useCompactView ? windowHeightCompact : windowHeightExpanded
         applicationWindow.x = useCompactView ? Screen.width - width : (Screen.width - width) / 2
         applicationWindow.y = useCompactView ? windowWidthCompact : (Screen.height - height) / 2
      }
   }

   width: applicationModel.useCompactView ? windowWidthCompact : windowWidthExpanded
   height: applicationModel.useCompactView ? windowHeightCompact : windowHeightExpanded

   // center horizontally
   x: applicationModel.useCompactView ? Screen.width - width : (Screen.width - width) / 2
   // center vertically
   y: applicationModel.useCompactView ? windowWidthCompact : (Screen.height - height) / 2

   background: Rectangle {
      id: background
      anchors.fill: parent

      //         gradient: Gradient {
      //            GradientStop {
      //               position: 0.0
      //               color: "#888888"
      //            }
      //            GradientStop {
      //               position: 0.5
      //               color: "#CCCCCC"
      //            }
      //            GradientStop {
      //               position: 1.0
      //               color: "#666666"
      //            }
      //         }
      //  we can also use a seemless image as background
      // from https://jryannel.wordpress.com/2010/02/16/background-for-our-examples/
      color: "transparent"
      Image {
         source: "images/brushedsteel1.jpg"
         fillMode: Image.Tile
         anchors.fill: parent
      }
   }

   Component.onCompleted: {
      topToolBar.btyRadioButton.checked = true
   }

   menuBar: TopMenuBar {
      id: topMenuBar
      appModel: applicationModel
      visible: !applicationModel.useCompactView
   }

   header: TopToolBar {
      id: topToolBar
      appModel: applicationModel
      specialistMode: applicationModel.specialistMode
      selectorID: applicationModel.selectorID
      psbEjectionBlm: tabPane.psbEjectionBlm
      visible: !applicationModel.useCompactView
   }

   TabPane {
      id: tabPane
      visible: !applicationModel.useCompactView
      appModel: applicationModel
      specialistMode: applicationModel.specialistMode
   }

   footer: QT14.StatusBar {
      visible: !applicationModel.useCompactView

      // add status label + progress bar and loging facilities here
   }

   CompactView {
      appModel: applicationModel
      visible: applicationModel.useCompactView
      selectorID: applicationModel.selectorID
   }
}
