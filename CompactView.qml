import QtQuick 2.4

CompactViewForm {

   onAppModelChanged: {
      ledPanel.appModel = appModel
   }

   compactViewButton.onClicked: {
      appModel.useCompactView = !appModel.useCompactView
   }
}
