import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Controls 1.4 as QT14

Item {
   id: item1
   width: 400
   height: 80
   property AppModel appModel
   property string selectorID
   property alias ledPanel: ledPanel
   property alias compactViewButton: compactViewButton
   property alias selectorIDLabel: selectorIDLabel

   Item {
      id: buttonRow
      height: selectorIDLabel.height + 2
      // spacing: 5
      anchors.right: parent.right
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.top: parent.top
      anchors.topMargin: 0
      QT14.Button {
         id: compactViewButton
         width: 20
         text: "+"
         anchors.left: parent.left
         anchors.leftMargin: 5
         anchors.top: parent.top
         anchors.topMargin: 1
      }

      Label {
         id: selectorIDLabel
         text: selectorID
         anchors.left: compactViewButton.right
         anchors.leftMargin: 5
         padding: 4
         font.bold: true
         font.pointSize: 13
         color: "blue"
         verticalAlignment: Text.AlignVCenter
         horizontalAlignment: Text.AlignLeft
      }
   }

   LedPanel {
      id: ledPanel
      anchors.left: parent.left
      anchors.top: buttonRow.bottom
      anchors.topMargin: 0
      anchors.leftMargin: 5
      blmStatusBoolArray: appModel.btyIcStatuses
      title: "???"
   }
}
