function buildMapKey(selector, devicepropertyField) {
   return selector + ":" + devicepropertyField
}

function isBlmSurveyOn(blmInterlock) {
   if (blmInterlock) {
      var surveyStatus = true
      for (var i = 0; i < blmInterlock.count; i++) {
         surveyStatus = surveyStatus & blmInterlock[i]
      }
      return surveyStatus
   }
   return false
}
