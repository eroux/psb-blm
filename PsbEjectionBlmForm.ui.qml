import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11

Item {
   id: psbEjectionBlm

   property AppModel appModel
   property bool specialistMode
   property alias btyColumn: btyColumn
   property alias btpColumn: btpColumn
   property alias psbBTPBlms: psbBTPBlms
   property alias psbBTYBlms: psbBTYBlms
   property alias currentState: psbEjectionBlm.state
   property variant psbEjectionTransferDevices
   property variant psbBtpDevices
   property variant psbBtyDevices

   Item {
      id: mainColumn
      anchors.rightMargin: 5
      anchors.leftMargin: 5
      anchors.bottomMargin: 5
      anchors.topMargin: 5
      anchors.fill: parent
      Item {
         id: psbEjectTransferColumn
         height: 260
         anchors.right: parent.right
         anchors.rightMargin: 0
         anchors.left: parent.left
         anchors.leftMargin: 0

         Label {
            id: psbEjectionTransferLabel
            text: qsTr("PSB Ejection / Transfer (BE / BT)")
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 15
            font.bold: true
         }

         Row {
            id: psbEjectionTransferBlms
            anchors.horizontalCenter: parent.horizontalCenter
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            anchors.top: psbEjectionTransferLabel.bottom
            anchors.topMargin: 5
            spacing: 2
            Repeater {
               model: psbEjectionTransferDevices
               Blm {
                  deviceName: modelData
                  appModel: psbEjectionBlm.appModel
                  specialistMode: psbEjectionBlm.specialistMode
               }
            }
         }
      }

      Item {
         id: switchableColumn
         y: 260
         height: 260
         anchors.right: parent.right
         anchors.rightMargin: 0
         anchors.left: parent.left
         anchors.leftMargin: 0
         anchors.top: psbEjectTransferColumn.bottom
         anchors.topMargin: 0

         Item {
            id: btyColumn
            anchors.fill: parent
            visible: true
            Label {
               id: btyLabel
               text: qsTr("BTY")
               anchors.horizontalCenter: parent.horizontalCenter
               verticalAlignment: Text.AlignVCenter
               horizontalAlignment: Text.AlignHCenter
               font.bold: true
               font.pointSize: 15
            }

            Row {
               id: psbBTYBlms
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.top: btyLabel.bottom
               anchors.topMargin: 5
               Layout.fillWidth: true
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               spacing: 2
               opacity: 1
               Repeater {
                  model: psbBtyDevices
                  Blm {
                     deviceName: modelData
                     appModel: psbEjectionBlm.appModel
                     specialistMode: psbEjectionBlm.specialistMode
                  }
               }
            }
         }

         Item {
            id: btpColumn
            anchors.fill: parent
            visible: false

            Label {
               id: btpLabel
               text: qsTr("BTP")
               anchors.horizontalCenter: parent.horizontalCenter
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
               font.pointSize: 15
               font.bold: true
            }

            Row {
               id: psbBTPBlms
               anchors.top: btpLabel.bottom
               anchors.topMargin: 5
               anchors.horizontalCenter: parent.horizontalCenter
               Layout.fillWidth: true
               Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
               spacing: 2
               opacity: 1
               Repeater {
                  model: psbBtpDevices

                  Blm {
                     deviceName: modelData
                     appModel: psbEjectionBlm.appModel
                     specialistMode: psbEjectionBlm.specialistMode
                  }
               }
            }
         }
      }

      BlmReset {
         id: blmReset
         anchors.horizontalCenter: parent.horizontalCenter
         anchors.top: switchableColumn.bottom
         anchors.topMargin: 5
         specialistMode: psbEjectionBlm.specialistMode
      }
   }

   states: [
      State {
         name: "BTY"
      },
      State {
         name: "BTP"
      }
   ]
}
