import QtQuick 2.4

TabPaneForm {
   width: parent ? parent.width : 500
   height: parent ? parent.height : 500

   onAppModelChanged: {
      psbEjectionBlm.appModel = appModel
      blmPpmTable.appModel = appModel
   }
}
