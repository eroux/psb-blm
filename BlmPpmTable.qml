import QtQuick 2.11
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4

import "Utils.js" as Utils

Item {
   id: item1
   width: parent ? parent.width : 600
   height: parent ? parent.height : 400

   property AppModel appModel

   TableView {
      id: tableView
      frameVisible: false
      sortIndicatorVisible: true
      alternatingRowColors: true
      model: sourceModel

      readonly property string surveyOn: "Survey ON"
      readonly property string surveyOff: "Survey OFF"
      readonly property string interlockOk: "NORMAL"
      readonly property string interlockBad: "ERROR"

      height: item1.height - refreshButton.height - 6
      horizontalScrollBarPolicy: 0
      headerVisible: true
      selectionMode: 0
      sortIndicatorColumn: 1
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0

      ///////////////////////////////////////////////////////////////
      // Table Column definition
      ///////////////////////////////////////////////////////////////
      TableViewColumn {
         id: userColumn
         title: "TGM User name"
         role: "user"
         movable: false
         resizable: false
         horizontalAlignment: Text.AlignHCenter
         width: 200
         delegate: userDelegate
      }

      TableViewColumn {
         id: surveyStatusColumn
         title: "Survey status"
         role: "surveyStatus"
         movable: false
         resizable: true
         horizontalAlignment: Text.AlignHCenter
         width: 200
         delegate: surveyDelegate
      }

      TableViewColumn {
         id: blmResetColumn
         title: "Blm Reset PPM"
         role: "blmReset"
         movable: false
         resizable: false
         horizontalAlignment: Text.AlignHCenter
         width: 120
         delegate: resetDelegate
      }

      TableViewColumn {
         id: interlockColumn
         title: "Interlock status HW"
         role: "interlockStatus"
         movable: false
         resizable: true
         horizontalAlignment: Text.AlignHCenter
         width: 200
         delegate: interlockDelegate
      }
      ///////////////////////////////////////////////////////////////
      // Renderers definition START
      ///////////////////////////////////////////////////////////////
      Component {
         id: userDelegate
         Text {
            anchors.verticalCenter: parent.verticalCenter
            anchors.fill: parent
            elide: styleData.elideMode
            text: styleData.value
            color: styleData.textColor
         }
      }

      Component {
         id: surveyDelegate
         Text {
            anchors.verticalCenter: parent.verticalCenter
            anchors.fill: parent
            anchors.centerIn: parent
            elide: styleData.elideMode
            text: styleData.value
            color: styleData.value === tableView.surveyOn ? "green" : "red"
            horizontalAlignment: Text.AlignHCenter
         }
      }
      Component {
         id: resetDelegate
         Button {
            text: "Reset"
            anchors.verticalCenter: parent.verticalCenter
            anchors.fill: parent
            onClicked: {
               console.log("Resetting BE.BLM/Reset#reset on " + sourceModel.get(
                              styleData.row).user + " @Row=" + styleData.row)
               appModel.cmwAccess.setPartial("BE.BLM/Reset", "reset",
                                             sourceModel.get(
                                                styleData.row).user, true)
            }
         }
      }
      Component {
         id: interlockDelegate
         Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.fill: parent
            anchors.centerIn: parent
            elide: styleData.elideMode
            text: styleData.value
            color: styleData.value === tableView.interlockOk ? "green" : "red"
            horizontalAlignment: Text.AlignHCenter
         }
      }

      ///////////////////////////////////////////////////////////////
      // ListModel definition
      ///////////////////////////////////////////////////////////////
      ListModel {
         id: sourceModel
         property variant plsArray: ["PSB.USER.AD", "PSB.USER.EAST1", "PSB.USER.EAST2", "PSB.USER.LHC1A", "PSB.USER.LHC1B", "PSB.USER.LHC2A", "PSB.USER.LHC2B", "PSB.USER.LHC3", "PSB.USER.LHC4", "PSB.USER.LHCINDIV", "PSB.USER.LHCPROBE", "PSB.USER.MD1", "PSB.USER.MD2", "PSB.USER.MD3", "PSB.USER.MD4", "PSB.USER.MD5", "PSB.USER.MD6", "PSB.USER.NORMGPS", "PSB.USER.NORMHRS", "PSB.USER.SFTPRO1", "PSB.USER.SFTPRO2", "PSB.USER.STAGISO", "PSB.USER.TOF", "PSB.USER.ZERO"]
         Component.onCompleted: {
            console.log("BlmPpmTable:++++++++ sourceModel CREATION")
            //console.log("plsArray.count=" + plsArray.length)
            for (var row = 0; row < plsArray.length; row++) {
               append({
                         "user": plsArray[row],
                         "surveyStatus": "-",
                         "interlockStatus": "-"
                      })
            }
            updateTable()
         }
      }
   } // Tableview END

   Button {
      id: refreshButton
      height: 30
      text: "refresh"
      anchors.right: parent.right
      anchors.rightMargin: 5
      anchors.left: parent.left
      anchors.leftMargin: 5
      anchors.top: tableView.bottom
      anchors.topMargin: 3
      onClicked: updateTable()
   }

   ///////////////////////////////////////////////////////////////
   // Global functions definition
   ///////////////////////////////////////////////////////////////
   /*
   * Update table survey(Col. 1) and interlock (Col. 3) columns contents
   */
   function updateTable() {
      // TODO :insist to ask Andres again to create GET metjhds which are returning a value
      var hasSurveyErrors = false
      var hasInterlockErrors = false
      for (var row = 0; row < sourceModel.count; row++) {
         var selectorID = sourceModel.get(row).user
         ///////////////////////////////////////////////////////////////
         // Survey column (1)
         ///////////////////////////////////////////////////////////////
         var surveyStatusKey = Utils.buildMapKey(
                  selectorID, "BE.BLM/Acquisition#chanStatus")
         //console.log("Doing GET on row [" + row + "]= " + surveyStatusKey)
         appModel.cmwAccess.get("BE.BLM/Acquisition", selectorID, null)
         var surveyStatus = appModel.cmwAccess.rdaDataAcquired[surveyStatusKey]
         if (surveyStatus) {
            var blmState = Utils.isBlmSurveyOn(surveyStatus)
            //console.log(blmState)
            sourceModel.get(
                     row).surveyStatus = blmState ? tableView.surveyOn : tableView.surveyOff
         } else {
            hasSurveyErrors = true
            // TODO uncomment the following line when Andres has fixed his code as at the momnet the errors for each parameter are stored in the global errors
            // sourceModel.get(row).surveyStatus = appModel.cmwAccess.errorRdaDataAcquired[surveyStatusKey]
            sourceModel.get(
                     row).surveyStatus = appModel.cmwAccess.globalExceptions
         }
         ///////////////////////////////////////////////////////////////
         //interlock column (3)
         ///////////////////////////////////////////////////////////////
         var interlockKey = Utils.buildMapKey(
                  selectorID, "BE.BLM/InterlockData#interlockStatus")
         //console.log("Doing GET on row [" + row + "]= " + interlockKey)
         appModel.cmwAccess.get("BE.BLM/InterlockData", selectorID, null)
         var interlock = appModel.cmwAccess.errorRdaDataAcquired[interlockKey]
         if (interlock)
            sourceModel.get(row).interlockStatus
                  = interlock ? tableView.interlockOk : tableView.interlockBad
         else {
            hasInterlockErrors = true
            // TODO uncomment the following line when Andres has fixed his code as at the momnet the errors for each parameter are stored in the global errors
            // sourceModel.get(row).interlockStatus = appModel.cmwAccess.errorRdaDataAcquired[interlockKey]
            sourceModel.get(
                     row).interlockStatus = appModel.cmwAccess.globalExceptions
         }
      }

      // in case of errors
      if (hasSurveyErrors)
         surveyStatusColumn.resizeToContents()
      if (hasInterlockErrors)
         interlockColumn.resizeToContents()
   }
}
