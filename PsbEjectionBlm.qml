import QtQuick 2.4

PsbEjectionBlmForm {
   width: parent ? parent.width : 500
   height: parent ? parent.height : 500

   onAppModelChanged: {
      psbEjectionTransferDevices = appModel.psbEjectionTransferDevices
      psbBtpDevices = appModel.psbBtpDevices
      psbBtyDevices = appModel.psbBtyDevices
   }

   readonly property int transitionSpeed: 400
   transitions: [
      Transition {
         from: "BTY"
         to: "BTP"
         NumberAnimation {
            target: btyColumn
            properties: "opacity"
            from: 1.0
            to: 0.0
            duration: transitionSpeed
         }
         NumberAnimation {
            target: btpColumn
            properties: "opacity"
            from: 0.0
            to: 1.0
            duration: transitionSpeed
         }
         PropertyAnimation {
            target: btyColumn
            properties: "visible"
            to: false
            duration: transitionSpeed
         }
         PropertyAnimation {
            target: btpColumn
            properties: "visible"
            to: true
            duration: transitionSpeed
         }
      },
      Transition {
         from: "BTP"
         to: "BTY"
         NumberAnimation {
            target: btpColumn
            properties: "opacity"
            from: 1.0
            to: 0.0
            duration: transitionSpeed
         }
         NumberAnimation {
            target: btyColumn
            properties: "opacity"
            from: 0.0
            to: 1.0
            duration: transitionSpeed
         }
         PropertyAnimation {
            target: btpColumn
            properties: "visible"
            to: false
            duration: transitionSpeed
         }
         PropertyAnimation {
            target: btyColumn
            properties: "visible"
            to: true
            duration: transitionSpeed
         }
      }
   ]
}
