import QtQuick 2.4
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0

Item {
   id: blmLed
   width: 100
   height: width

   ToolTip {
      parent: blmLed
      visible: mouseArea.containsMouse
      text: qsTr(blmName + ": " + (blmStatus ? "Ok" : "Error"))
      background: Rectangle {
         border.color: "black"
         border.width: 1
         color: blmStatus ? ledOnColor : ledOffColor
      }
   }
   MouseArea {
      id: mouseArea
      anchors.fill: parent
      hoverEnabled: true
   }

   property string blmName
   property bool blmStatus
   readonly property int glowRadiusDefault: 10
   readonly property double glowSpreadDefault: 0.1
   readonly property color ledOnColor: "#00e600" // ~green
   readonly property color ledOffColor: "#e60000" // ~red

   // Uncomment this to check the glow effect with a black background using the qmlscene viewer
   //Rectangle {
   //         anchors.fill: parent
   //         color: "black"
   //      }
   Item {
      id: ledOn
      anchors.fill: parent
      visible: blmStatus
      Rectangle {
         id: ledRectangleOn
         anchors.fill: parent
         anchors.centerIn: parent
         radius: width / 2
         border.width: 0
      }

      RectangularGlow {
         anchors.fill: ledRectangleOn
         glowRadius: glowRadiusDefault
         spread: glowSpreadDefault
         color: "white"
         cornerRadius: ledRectangleOn.radius + glowRadius
      }

      RadialGradient {
         anchors.fill: ledRectangleOn
         source: ledRectangleOn
         gradient: Gradient {
            GradientStop {
               position: 0.0
               color: "#F0F0F0"
            }
            GradientStop {
               position: 0.5
               color: ledOnColor
            }
            GradientStop {
               position: 1.0
               color: "#F0F0F0"
            }
         }
      }
   }

   Item {
      id: ledOff
      anchors.fill: parent
      visible: !blmStatus
      Rectangle {
         id: ledRectangleOff
         anchors.fill: parent
         anchors.centerIn: parent
         radius: width / 2
         border.width: 0
      }

      RectangularGlow {
         anchors.fill: ledRectangleOff
         glowRadius: glowRadiusDefault
         spread: glowSpreadDefault
         color: "white"
         cornerRadius: ledRectangleOff.radius + glowRadius
      }

      RadialGradient {
         anchors.fill: ledRectangleOff
         source: ledRectangleOff
         gradient: Gradient {
            GradientStop {
               position: 0.0
               color: "#F0F0F0"
            }
            GradientStop {
               position: 0.5
               color: ledOffColor
            }
            GradientStop {
               position: 1.0
               color: "#F0F0F0"
            }
         }
      }
   }
}
