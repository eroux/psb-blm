import QtQuick 2.4

TopMenuBarForm {

   Component.onCompleted: {
      specialistModeAction.checked = appModel.specialistMode
   }
   quitAction.onTriggered: Qt.quit()

   // reset BLMs on all TGM users
   resetAllUsersAction.onTriggered: {

   }

   // switch to specialist mode
   specialistModeAction.onTriggered: {
      appModel.specialistMode = specialistModeAction.checked
      console.log("Specialist mode=[" + appModel.specialistMode + "]")
   }

   // display about box
   aboutAction.onTriggered: {

   }
}
