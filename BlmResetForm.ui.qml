import QtQuick 2.11
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0

Item {
   id: item1

   property AppModel appModel
   property alias cyclelabel: cycleLabel.text
   property alias resetButton: resetButton
   property alias resetAllButton: resetAllButton
   property alias countLabel: countLabel.text
   property alias maxCountlabel: maxCountlabel.text
   property alias blmStateImage: blmStateImage.source
   property alias specialistMode: specialistRow.visible

   height: 260

   Item {
      id: column1
      anchors.fill: parent
      Row {
         id: titleLabels
         anchors.rightMargin: 0
         anchors.leftMargin: 0
         transformOrigin: Item.Center
         anchors.right: parent.right
         anchors.left: parent.left
         spacing: 5
         anchors.top: parent.top
         anchors.topMargin: 15
         Label {
            id: titleLabel
            height: titleLabel.height
            text: "Bad consecutive cycle count for user: "
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 13
            font.bold: true
         }
         Label {
            id: cycleLabel
            text: qsTr("???")
            anchors.verticalCenter: parent.verticalCenter
            anchors.top: titleLabel.verticalCenter
            anchors.topMargin: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignLeft
            font.pointSize: 13
            font.bold: true
            color: "blue"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: titleLabel.right
            anchors.leftMargin: 5
         }
      }

      Item {
         id: blmResetItem
         width: 180
         height: 200
         anchors.horizontalCenter: parent.horizontalCenter
         anchors.top: titleLabels.bottom
         anchors.topMargin: 14

         Rectangle {
            id: rectangle
            color: "#ffffff"
            radius: 15
            border.width: 2
            anchors.fill: parent
         }

         //         RectangularGlow {
         //            id: effect
         //            anchors.fill: rectangle
         //            glowRadius: 10
         //            spread: 0.2
         //            color: "white"
         //            cornerRadius: rectangle.radius + glowRadius
         //         }
         Glow {
            anchors.fill: rectangle
            radius: 10
            spread: 0.2
            samples: 17
            color: "white"
            source: rectangle
         }

         DropShadow {
            anchors.fill: rectangle
            horizontalOffset: 3
            verticalOffset: 3
            radius: 10
            samples: 17
            color: "#80000000"
            source: rectangle
         }

         //         InnerShadow {
         //            anchors.fill: rectangle
         //            radius: 10.0
         //            samples: 16
         //            horizontalOffset: -3
         //            verticalOffset: 3
         //            color: "#b0000000"
         //            source: rectangle
         //         }
         Item {
            id: column
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            anchors.topMargin: 10
            anchors.fill: parent

            Row {
               id: specialistRow
               anchors.right: parent.horizontalCenter
               anchors.rightMargin: -56
               anchors.left: parent.horizontalCenter
               anchors.leftMargin: -56
               anchors.top: parent.top
               anchors.topMargin: 0
               spacing: 2

               TextField {
                  id: textField
                  width: 50
                  height: 20
                  text: qsTr("")
                  font.pointSize: 9
                  leftPadding: 2
                  padding: 1
               }

               Button {
                  id: setButton
                  width: 30
                  height: 20
                  text: qsTr("Set")
                  rightPadding: 0
                  leftPadding: 0
                  padding: 0
                  spacing: 0
               }

               Button {
                  id: initButton
                  width: 30
                  height: 20
                  text: qsTr("Init")
                  rightPadding: 0
                  leftPadding: 0
                  padding: 0
               }
            }

            Row {
               id: countLabels
               spacing: 5
               anchors.horizontalCenterOffset: 0
               anchors.top: specialistRow.bottom
               anchors.topMargin: 5
               anchors.horizontalCenter: parent.horizontalCenter

               Label {
                  id: countLabel
                  color: "#78ade3"
                  text: qsTr("???")
                  font.pointSize: 13
                  font.bold: true
               }

               Label {
                  id: label
                  text: qsTr("/")
                  font.pointSize: 13
                  font.bold: true
               }

               Label {
                  id: maxCountlabel
                  color: "#8e6108"
                  text: qsTr("???")
                  font.bold: true
                  font.pointSize: 13
               }
            }
            Image {
               id: blmStateImage
               width: 100
               height: 100
               anchors.horizontalCenterOffset: 0
               anchors.horizontalCenter: countLabels.horizontalCenter
               anchors.top: countLabels.bottom
               anchors.topMargin: 5
               source: "images/Ok.png"
            }

            Row {
               id: buttonRow
               spacing: 4
               anchors.top: blmStateImage.bottom
               anchors.topMargin: 5
               Button {
                  id: resetButton
                  width: 70
                  height: 20
                  text: "Reset"
                  padding: 0
                  font.pointSize: 13
               }

               Button {
                  id: resetAllButton
                  width: 90
                  height: 20
                  text: "Reset All"
                  padding: 0
                  font.pointSize: 13
               }
            }
         }
      }

      Row {
         id: bottomLabels
         width: 640
         anchors.horizontalCenter: parent.horizontalCenter
         anchors.topMargin: 15
         anchors.top: blmResetItem.bottom
         Label {
            id: btbct10TitleLabel
            height: btbct10TitleLabel.height
            text: "BT.BCT10 [E10]:"
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            anchors.topMargin: 0
            horizontalAlignment: Text.AlignHCenter
            anchors.top: parent.top
            font.pointSize: 13
         }

         Label {
            id: btbct10ValueLabel
            color: "#0000ff"
            text: qsTr("???")
            anchors.verticalCenter: btbct10TitleLabel.verticalCenter
            verticalAlignment: Text.AlignVCenter
            anchors.left: btbct10TitleLabel.right
            anchors.leftMargin: 5
            font.bold: true
            anchors.topMargin: 0
            anchors.rightMargin: 0
            anchors.right: parent.right
            horizontalAlignment: Text.AlignLeft
            anchors.top: parent.top
            font.pointSize: 13
         }
         spacing: 5
      }
   }
}
