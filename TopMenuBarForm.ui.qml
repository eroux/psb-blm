import QtQuick 2.11
import QtQuick.Controls 2.4

MenuBar {
   id: menuBar
   property AppModel appModel
   property alias quitAction: quitAction
   property alias resetAllUsersAction: resetAllUsersAction
   property alias specialistModeAction: specialistModeAction
   property alias aboutAction: aboutAction

   Menu {
      title: qsTr("&File")
      Action {
         id: quitAction
         text: qsTr("&Quit")
      }
   }
   Menu {
      title: qsTr("&Control")
      Action {
         id: resetAllUsersAction
         text: qsTr("Reset All Users")
      }
      Action {
         id: specialistModeAction
         text: qsTr("Specialist mode")
         checkable: true
         // checked: false
      }
   }
   Menu {
      title: qsTr("&Help")
      Action {
         id: aboutAction
         text: qsTr("&About")
      }
   }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
