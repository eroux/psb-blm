import QtQuick 2.0
import QtQml.Models 2.1

import RDA3 1.0

ObjectModel {

   property alias cmwAccess: cmwAccess

   //*** IMPORTANT :ADDING THE QT OBJECT)
   RdaGeneralActionsWrapper {
      id: cmwAccess

      //onRdaDataAcquiredChanged: {

      //         blmStatuses = cmwAccess.rdaDataAcquired[btyStatusParameter]
      //         if (blmStatuses) {
      //            console.log("AppModel.qml::onRdaDataAcquiredChanged - BLM value=["
      //                        + blmStatuses.length + "]" + blmStatuses)
      //            for (var propertyIndex = 0; propertyIndex < blmStatuses.length; propertyIndex++) {
      //               console.log(
      //                        "Received[" + propertyIndex + "]= " + blmStatuses[propertyIndex])
      //            }
      //            //    blmStatuses = btyAlarmStatusValue
      //         }
      //  }

      // apparently the following two lines are displaying errors in QtCreator
      //  but they are working fine (I do not understand what is the problem )
      onErrorRdaDataAcquiredChanged: {
         console.log(
                  " Specific error: " + cmwAccess.errorRdaDataAcquired[btyIcStatusParameter])
      }

      onGlobalExceptionsChanged: {
         console.log(" global error: " + cmwAccess.globalExceptions)
      }
   }

   property variant btyIcStatuses: cmwAccess.rdaDataAcquired[btyIcStatusParameter]

   readonly property variant psbEjectionTransferDevices: ["BE.BLM-S", "BE.BLM-I", "BT.BLM10", "BT.BLMCO", "BT.BLM20", "BT.BLM30", "BT.BLM40", "BTM.BLM101", "BTM.BLM10"]

   readonly property variant psbBtyDevices: ["BTY.BLMIB.101.T", "BTY.BLMIB.108.L", "BTY.BLMIB.113.L", "BTY.BLMIB.120.L", "BTY.BLMIB.127.L", "BTY.BLMIB.151.L", "BTY.BLMIB.154.L"]

   readonly property variant psbBtpDevices: ["BTP.BLM00", "BTP.BLM10"]

   readonly property string btyDevice: "BT.BLMINJ"

   readonly property string blmCYCLELOSS: "Acquisition#lossRS3"
   readonly property string blmTHRESHOLD_AQN: "Acquisition#swThresholdsRS3"
   readonly property string blmSTATUS: "Acquisition#swDumpStatusRS3"
   readonly property string blmTHRESHOLD_CCV: "SWThresholds#swThresholdsRS3"
   readonly property string blmLossSum: "Acquisition#lossCycleSum"
   readonly property string blmChannelStatus: "Acquisition#channelStatus"
   readonly property string btyIcStatus: "Acquisition#swDumpStatusRS3"

   // custom type array value
   readonly property string btyChannelStatusParameter: btyDevice + "/" + blmChannelStatus
   // boolean array value
   readonly property string btyIcStatusParameter: btyDevice + "/" + btyIcStatus
   // double array value
   readonly property string btyCycleLossParameter: btyDevice + "/" + blmCYCLELOSS
   // scalar value
   readonly property string btyLossParameter: btyDevice + "/" + blmLossSum

   readonly property variant blmProperties: ["Acquisition"]

   //, "SWThresholds"]
   property bool specialistMode: false
   property bool useCompactView: false
   property string selectorID: Qt.application.arguments[1]

   onSelectorIDChanged: {
      cmwAccess.setSelector(selectorID)
   }

   Component.onCompleted: {
      var deviceProperty
      // for (var deviceIndex = 0; deviceIndex < psbEjectionTransferDevices.length; deviceIndex++) {
      for (var propertyIndex = 0; propertyIndex < blmProperties.length; propertyIndex++) {
         deviceProperty = btyDevice + "/" + blmProperties[propertyIndex]
         console.log(
                  "AppModel.qml::onCompleted - Registering " + deviceProperty)
         //cmwAccess.subscribe(deviceProperty, selectorID, null)
         cmwAccess.registerDeviceProperty(deviceProperty)
      }
      // }
      cmwAccess.setState(RdaGeneralActionsWrapper.MONITOR)
   }

   Component.onDestruction: {
      var deviceProperty
      console.log("=================== APPLICATION EXIT START ===============")
      // for (var deviceIndex = 0; deviceIndex < psbEjectionTransferDevices.length; deviceIndex++) {
      //      for (var propertyIndex = 0; propertyIndex < blmProperties.length; propertyIndex++) {
      //         deviceProperty = btyDevice + "/" + blmProperties[propertyIndex]
      //         console.log("AppModel.qml::onDestruction - STOPPING  subscription on "
      //                     + deviceProperty + " with " + selectorID + " selector")
      //         // uncomment this line was soon as we manage to support virtual devices...
      //         cmwAccess.unsubscription(deviceProperty)
      //      }
      cmwAccess.setState(RdaGeneralActionsWrapper.IDLE)
      console.log("=================== APPLICATION EXIT END ===============")
   }
}
