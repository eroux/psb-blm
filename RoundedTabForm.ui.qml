import QtQuick 2.4

Item {
   width: 100
   height: 30

   property alias color: roundRect.color
   property alias border: roundRect.border

   Rectangle {
      anchors.fill: parent
      id: roundRect
      radius: 10
      color: "#888888"
   }

   Rectangle {
      id: squareRect
      color: roundRect.color
      anchors.bottomMargin: 0
      anchors.leftMargin: roundRect.border.width
      anchors.rightMargin: roundRect.border.width
      y: roundRect.radius
      height: parent.height - roundRect.radius
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
   }
}
