import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls 1.4 as QT14

import RDA3 1.0

ToolBar {
   id: toolBar
   property AppModel appModel
   property bool specialistMode
   property string selectorID
   property PsbEjectionBlm psbEjectionBlm
   property alias btpRadioButton: btpRadioButton
   property alias btyRadioButton: btyRadioButton
   property alias getButton: getButton
   property alias monitorButton: monitorButton
   property alias stopButton: stopButton
   property alias compactViewButton: compactViewButton
   // add 3D effect to the toolbarbackground
   Rectangle {
      color: "transparent"
      border.color: "#CCCCCC"
      anchors.fill: parent
      gradient: Gradient {
         GradientStop {
            position: 0.0
            color: "#888888"
         }
         GradientStop {
            position: 0.9
            color: "#CCCCCC"
         }
         GradientStop {
            position: 1.0
            color: "#666666"
         }
      }
   }

   Item {
      id: row
      anchors.verticalCenter: parent.verticalCenter
      anchors.top: parent.top
      anchors.right: parent.right
      anchors.left: parent.left
      // spacing: 3
      anchors.leftMargin: 8

      QT14.Button {
         id: compactViewButton
         width: 20
         text: "-"
         anchors.verticalCenter: parent.verticalCenter
      }

      Label {
         id: selectorIDLabel
         text: selectorID
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: compactViewButton.right
         anchors.leftMargin: 0
         padding: 4
         font.bold: true
         font.pointSize: 13
         color: "blue"
         verticalAlignment: Text.AlignVCenter
         horizontalAlignment: Text.AlignLeft
      }

      QT14.Button {
         id: getButton
         text: "Get"
         iconSource: "images/AccessStateGet.gif"
         anchors.left: selectorIDLabel.right
         anchors.verticalCenter: parent.verticalCenter
         enabled: appModel.cmwAccess.state === RdaGeneralActionsWrapper.IDLE
      }

      QT14.Button {
         id: monitorButton
         text: "Monitor"
         iconSource: "images/AccessStateMonitor.gif"
         anchors.left: getButton.right
         anchors.leftMargin: 0
         anchors.verticalCenter: parent.verticalCenter
         enabled: appModel.cmwAccess.state === RdaGeneralActionsWrapper.IDLE
      }

      QT14.Button {
         id: stopButton
         text: "Stop"
         iconSource: "images/AccessStateStop.gif"
         anchors.left: monitorButton.right
         anchors.leftMargin: 0
         anchors.verticalCenter: parent.verticalCenter
         enabled: appModel.cmwAccess.state === RdaGeneralActionsWrapper.MONITOR
      }

      ToolSeparator {
         id: toolSeparator
         height: parent.height
         anchors.left: stopButton.right
         anchors.leftMargin: 0
         anchors.verticalCenter: parent.verticalCenter
      }

      Label {
         id: transferLineSelectionTitle
         text: "Transfer line selection: "
         anchors.verticalCenter: parent.verticalCenter
         padding: 4
         anchors.left: toolSeparator.right
         anchors.leftMargin: 0
         font.bold: true
         font.pointSize: 10
         verticalAlignment: Text.AlignVCenter
         horizontalAlignment: Text.AlignLeft
      }
      RadioButton {
         id: btpRadioButton
         text: qsTr("BTP")
         anchors.left: transferLineSelectionTitle.right
         anchors.leftMargin: 0
         anchors.verticalCenter: parent.verticalCenter
      }
      RadioButton {
         id: btyRadioButton
         text: qsTr("BTY")
          anchors.left: btpRadioButton.right
         anchors.leftMargin: 0
         anchors.verticalCenter: parent.verticalCenter
      }

      Label {
         id: specialistModeTitle
         text: "Specialist mode: "
         anchors.left: btyRadioButton.right
         anchors.leftMargin: 0
         anchors.verticalCenter: parent.verticalCenter
         padding: 4
         font.bold: true
         font.pointSize: 10
         verticalAlignment: Text.AlignVCenter
         horizontalAlignment: Text.AlignLeft
      }
      Label {
         id: specialistModeStatus
         text: "On"
         anchors.left: specialistModeTitle.right
         anchors.leftMargin: 0
         anchors.verticalCenter: parent.verticalCenter
         padding: 4
         color: "red"
         font.bold: true
         font.pointSize: 10
         verticalAlignment: Text.AlignVCenter
         horizontalAlignment: Text.AlignLeft
         visible:  specialistMode
      }
      Label {
         text: "Off"
         anchors.left: specialistModeTitle.right
         anchors.leftMargin: 0
         anchors.verticalCenter: parent.verticalCenter
         padding: 4
         color: "#555555"
         font.pointSize: 10
         verticalAlignment: Text.AlignVCenter
         horizontalAlignment: Text.AlignLeft
         visible: !specialistMode
      }
   }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
