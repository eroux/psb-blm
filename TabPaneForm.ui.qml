import QtQuick 2.4
import QtQuick.Controls 1.4 as QT14
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Item {
id:item1
   property AppModel appModel
   property bool specialistMode
   property alias psbEjectionBlm: psbEjectionBlm
   property alias blmPpmTable: blmPpmTable
   readonly property color selectedTabBorderColor: "#80dfff"
   readonly property color selectedTabColor: "#777777"
   readonly property color unselectedTabColor: "#CCCCCC"

   TabBar {
      id: tabBar
      anchors.top: parent.top
      anchors.topMargin: 0
      anchors.left: parent.left
      anchors.right: parent.right
      background: Rectangle {
         color: "#EEEEEE"
      }
      TabButton {
         text: qsTr("PSB Ejection BLM")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 0 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 0 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
      TabButton {
         text: qsTr("Ring BLM")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 1 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 1 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
      TabButton {
         text: qsTr("Blm PPM Table")
         font.bold: true
         font.pointSize: 12
         width: implicitWidth
         background: RoundedTab {
            border.width: 2
            border.color: tabBar.currentIndex == 2 ? selectedTabBorderColor : unselectedTabColor
            color: tabBar.currentIndex == 2 ? selectedTabColor : unselectedTabColor
            width: parent.width
         }
      }
   }

   StackLayout {
      // anchors.fill.width: parent.width
      anchors.top: tabBar.bottom
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.topMargin: 0
      currentIndex: tabBar.currentIndex

      PsbEjectionBlm {
         id: psbEjectionBlm
         specialistMode:item1.specialistMode
      }

      Item {
         id: psbRingTab
      }

      BlmPpmTable {
         id: blmPpmTable
      }
   }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
