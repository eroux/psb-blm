QT += quick

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    rdadevicepropertyaccess.cpp \
    rdageneralactionswrapper.cpp

HEADERS += \
    rdadevicepropertyaccess.h \
    rdageneralactionswrapper.h

RESOURCES += qml.qrc

#ACC_PLUGINS_PATH=/acc/java/data/Qt/libs/plugins
### Additional import path used to resolve QML modules in Qt Creator's code model
### ERIC: following variables does not seems to be used by Qt Creator as we have to add the
### QML2_IMPORT_PATH in the projectś build variables so the RDA3 plugin is found by Qt Creator.
#QML_IMPORT_PATH =$$ACC_PLUGINS_PATH
#QML2_IMPORT_PATH =$$ACC_PLUGINS_PATH
#QML_DESIGNER_IMPORT_PATH =$$ACC_PLUGINS_PATH

#RDA3_PATH = $$ACC_PLUGINS_PATH/RDA3
#INCLUDEPATH += $$RDA3_PATH/include/
#DEPENDPATH +=  $$RDA3_PATH/include/
#LIBS += -L$$RDA3_PATH -lrda3plugin

#QTPLUGIN += rda3plugin
#message(qmake ACC_PLUGINS_PATH=$$ACC_PLUGINS_PATH)
#message(qmake        RDA3_PATH=$$RDA3_PATH)

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

# disable all CMW-RDA warnings
QMAKE_CXXFLAGS += -Wno-deprecated

INCLUDEPATH += /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/include
DEPENDPATH += /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/include

LIBS += -L/acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/ \
        -lcmw-rda3-cern \
        -lcmw-data-util \
        -lcmw-rda \
        -lcmw-rbac \
        -lcmw-rda3 \
        -lcmw-directory-client \
        -lcmw-data \
        -lcmw-serializer \
        -lcmw-log \
        -lcmw-util \
        -lomniORB4 \
        -lomnithread \
        -liceutil \
        -lzmq \
        -lboost_1_54_0_chrono \
        -lboost_1_54_0_thread \
        -lboost_1_54_0_system \
        -lboost_1_54_0_atomic \
        -lboost_1_54_0_filesystem \
        -lcurl -lcrypto -lrt

PRE_TARGETDEPS += /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-rda3-cern.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-data-util.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-rda.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-rbac.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-rda3.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-directory-client.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-data.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-serializer.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-log.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libcmw-util.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libomniORB4.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libomnithread.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libiceutil.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libzmq.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libboost_1_54_0_chrono.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libboost_1_54_0_thread.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libboost_1_54_0_system.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libboost_1_54_0_atomic.a \
                  /acc/local/L867/cmw/cmw-rda3-cern/2.1.0/lib/libboost_1_54_0_filesystem.a
