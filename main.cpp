#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "rdageneralactionswrapper.h"
#include "rdadevicepropertyaccess.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    // to avoid QML errors in import
    qmlRegisterType<RdaGeneralActionsWrapper>("RDA3", 1, 0, "RdaGeneralActionsWrapper");

    QQmlApplicationEngine engine;
    for(QString path : engine.importPathList())
    {
        qDebug("import path= %s", path.toStdString().c_str());
    }
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
