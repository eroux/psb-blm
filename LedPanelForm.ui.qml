import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Controls 1.4 as QT14

Item {
   id: ledPanelId
   width: 180
   height: 40

   property AppModel appModel
   property string title
   property variant blmStatusBoolArray
   property alias blmLedRepeater: blmLedRepeater

   //   Rectangle {
   //      anchors.fill: parent
   //      border.width: 2
   //      border.color: "black"
   //      color: "transparent"
   //   }
   Label {
      id: titleLabel
      text: title
      anchors.verticalCenter: parent.verticalCenter
      verticalAlignment: Text.AlignVCenter
      horizontalAlignment: Text.AlignLeft
      font.bold: true
      font.pointSize: 12
   }

   Row {
      id: psbBtyBlms
      anchors.verticalCenter: parent.verticalCenter
      anchors.leftMargin: 10
      spacing: 2

      // we cannot use a rewpeater as there is no way to cretae a dynamic id
      // so there is no way to set a value to the led :((
      Repeater {
         id: blmLedRepeater
         model: appModel.psbBtyDevices
         BlmLed {
            blmName: modelData
            width: 20
         }
      }
   }
}
