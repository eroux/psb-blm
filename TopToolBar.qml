import QtQuick 2.11

import RDA3 1.0

TopToolBarForm {

   compactViewButton.onClicked: {
      appModel.useCompactView = !appModel.useCompactView
   }

   // appModel.rda3Instance.onStateChanged:
   btpRadioButton.onCheckedChanged: {
      psbEjectionBlm.currentState = btpRadioButton.text
      // TODO send this to status bar log
      console.log("Current State=[" + psbEjectionBlm.currentState + "]")
   }

   btyRadioButton.onCheckedChanged: {
      psbEjectionBlm.currentState = btyRadioButton.text
      // TODO send this to status bar log
      console.log(
               "Current radio button selection=[" + psbEjectionBlm.currentState + "]")
   }

   getButton.onEnabledChanged: {
      if (getButton.enabled)
         getButton.iconSource = "images/AccessStateGet.gif"
      else
         getButton.iconSource = "images/AccessStateGetDisabled.gif"
   }
   getButton.onClicked: {
      console.log(getButton.text)
      appModel.cmwAccess.setState(RdaGeneralActionsWrapper.GET)
   }

   monitorButton.onEnabledChanged: {
      if (monitorButton.enabled)
         monitorButton.iconSource = "images/AccessStateMonitor.gif"
      else
         monitorButton.iconSource = "images/AccessStateMonitorDisabled.gif"
   }
   monitorButton.onClicked: {
      console.log(monitorButton.text)
      appModel.cmwAccess.setState(RdaGeneralActionsWrapper.MONITOR)
   }

   stopButton.onEnabledChanged: {
      if (stopButton.enabled)
         stopButton.iconSource = "images/AccessStateStop.gif"
      else
         stopButton.iconSource = "images/AccessStateStopDisabled.gif"
   }
   stopButton.onClicked: {
      console.log(stopButton.text)
      appModel.cmwAccess.setState(RdaGeneralActionsWrapper.IDLE)
   }
}
